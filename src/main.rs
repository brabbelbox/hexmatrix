//! # 16x16 Hexmatrix OSC-Jack-Mixer

#[macro_use]
extern crate rouille;
use arr_macro::arr;
use clap::{App, Arg};
use jack;
use jack::{AudioIn, AudioOut, Port};
use rayon::prelude::*;
use serde::{Deserialize, Serialize};
use std::io;
use std::ops::Mul;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::thread;
use std::time::{Duration, Instant};
use ebur128::EbuR128;
use atomic_float::AtomicF32;

/*
TODO:
- [ ] Document dependecies
- [ ] Performance improvements:
    - [ ] implement performance monitoring
    - [ ] Flag volume modulations as dirty
    - [x] f32.mul_add() or f32.to_bits() / f32.from_bits()
    - [x] only calculate connected ports https://docs.rs/jack/0.7.1/jack/struct.Port.html#method.connected_count
- [x] Support other Buffers than 1024
- [ ]  Features:
    - [ ] Flags to control printing (--quiet etc)
    - [ ] Option to set initial values
    - [ ] Option to limit certain inputs/outputs to certain volumes
    - [ ] Option to ignore certain modulations
    - [ ] GUI/TLI to see matrix and current volumes
    - [ ] OSC: Per Channel Solo/Mute/Invert
    - [ ] OSC: Output Volumes
    - [ ] OSC: Output Levels
*/

// Number of inputs and outputs.
// Careful: this needs to be changed in many places
const N_INPUTS: usize = 16;
const N_OUTPUTS: usize = 16;

// Atomic Variables for Shared Mutability of Modulation values
// One per Input, with n values where n is the number of outputs
// Incoming float values get scaled to a unsigned usize (e.g. 64 bit)
// Static arrays are used here for maximum performance
// (No: it is not possible to nest arr! macro calls. yet.)
static VOLS: [[AtomicUsize; N_INPUTS]; N_OUTPUTS] = [
    arr![AtomicUsize::new(0); 16],
    arr![AtomicUsize::new(0); 16],
    arr![AtomicUsize::new(0); 16],
    arr![AtomicUsize::new(0); 16],
    arr![AtomicUsize::new(0); 16],
    arr![AtomicUsize::new(0); 16],
    arr![AtomicUsize::new(0); 16],
    arr![AtomicUsize::new(0); 16],
    arr![AtomicUsize::new(0); 16],
    arr![AtomicUsize::new(0); 16],
    arr![AtomicUsize::new(0); 16],
    arr![AtomicUsize::new(0); 16],
    arr![AtomicUsize::new(0); 16],
    arr![AtomicUsize::new(0); 16],
    arr![AtomicUsize::new(0); 16],
    arr![AtomicUsize::new(0); 16],
];

static METERS: [AtomicF32; N_INPUTS] = arr![AtomicF32::new(0.0); 16];


static INPUT_CONNECTIONS: AtomicUsize = AtomicUsize::new(0);
static OUTPUT_CONNECTIONS: AtomicUsize = AtomicUsize::new(0);

fn get_volume_for_input_output_pair(input_index: usize, output_index: usize) -> f32 {
    VOLS[input_index][output_index].load(Ordering::Relaxed) as f32 / usize::MAX as f32
}

fn port_has_connections<T>(port: &Port<T>) -> bool {
    match port.connected_count() {
        Ok(count) => count > 0,
        _ => true,
    }
}

pub struct Connection {
}

#[derive(Debug, Serialize, Deserialize)]
struct VolumeData {
    volumes: Vec<Vec<f32>>,
}

impl Connection {
    pub fn from_vec(v: Vec<&str>) -> Self {
        let connections: Vec<Connector> = v
            .iter()
            .map(|c| {
                c.split(",")
                    .collect::<Vec<&str>>()
                    .iter()
                    .map(|x| x.trim())
                    .map(|x| Connector::new(x))
                    .collect::<Vec<Connector>>()
            })
            .flatten()
            .collect::<Vec<Connector>>();

        if connections.first().unwrap().is_own() {
            // Outgoing Connections
        } else {
            // Incoming Connections
        }

        Self {
        }
    }
}

pub struct Connector {
    name: String,
}

impl Connector {
    pub fn new<S: AsRef<str>>(stringlike: S) -> Self {
        Self {
            name: stringlike.as_ref().to_string(),
        }
    }

    pub fn is_own(&self) -> bool {
        !self.name.contains(":") || self.name == "all"
    }

    pub fn is_other(&self) -> bool {
        !self.is_own()
    }

    pub fn is_own_input(&self) -> bool {
        self.is_own() && self.name.starts_with("in_")
    }

    pub fn is_own_output(&self) -> bool {
        self.is_own() && self.name.starts_with("out_")
    }
}

fn main() {
    let matches = App::new(option_env!("CARGO_PKG_NAME").unwrap())
        .version(option_env!("CARGO_PKG_VERSION").unwrap())
        .about(option_env!("CARGO_PKG_DESCRIPTION").unwrap())
        .help_template("{bin} ({version})\n{about}\n\nUsage:\n    {usage}\n\nTo send a certain amount of one input to a output send a OSC message that looks like this: /ch/1/send/16 <volume>\nThis would set the send volume of input 1 to output 16 to the value of <volume>. A value of 0.0 would be off, a value of 1.0 would result in a unattenuated signal. Values above 1.0 result in amplification.\n\nOSC-Addresses:\n    /ch/<in>/send/<out> <volume>     Set the send volume (in → out)\n\nOptions:\n{options}")
        .arg(Arg::new("--host")
            .short('h')
            .long("--host")
            .value_name("HOST")
            .help(format!("Set IP-address of {}", option_env!("CARGO_PKG_NAME").unwrap()).as_str())
            .takes_value(true))
        .arg(Arg::new("--port")
            .short('p')
            .long("--port")
            .value_name("INPORT")
            .help("Set port (OSC)")
            .takes_value(true))
        .arg(Arg::new("--name")
            .short('n')
            .long("--name")
            .value_name("NAME")
            .help("Set the name of the jack node")
            .takes_value(true))
        .get_matches();

    let host = matches.value_of("--host").unwrap_or("127.0.0.1");
    let port = matches.value_of("--port").unwrap_or("1234");

    let nodename = matches.value_of("--name").unwrap_or("Hexmatrix");

    // Create new jack client (visible e.g. in Catia)
    let (client, _status) = jack::Client::new(nodename, jack::ClientOptions::NO_START_SERVER)
        .expect("Couldn't connect to jack server. Not running?");

    // Open n Jack Input Ports via arr! macro.
    let mut i = 0u16;
    let inputs: [Port<AudioIn>; 16] = arr![
        client.register_port(
            format!("in_{}", {i += 1; i}).as_str(),
            jack::AudioIn::default())
              .unwrap();
        16];

    // Open n Jack Output Ports via arr! macro.
    i = 0;
    let mut outputs: [Port<AudioOut>; 16] = arr![
        client.register_port(
            format!("out_{}", {i += 1; i}).as_str(),
            jack::AudioOut::default())
                .unwrap();
        16];

    // EbuR128 meters to calculate true peak value
    let mut input_meters: [EbuR128; 16] = arr![
        EbuR128::new(1, 48_0000, ebur128::Mode::TRUE_PEAK).unwrap();
    16];

    // reduce the last known peak every 100ms
    thread::spawn(move || {
        loop {
            let interval = Duration::from_millis(100);
            let mut next_time = Instant::now() + interval;

            for i in 0..16 {
                METERS[i].fetch_update(
                    Ordering::Relaxed,
                    Ordering::Relaxed,
                    move |old_meter: f32| {
                        Some(old_meter.mul(0.9))
                    },
                ).unwrap();

            }

            thread::sleep(next_time - Instant::now());
            next_time += interval;
        }
    });


    let buffersize = client.buffer_size() as usize;

    // Is called asynchronously. Moves/mixes the actual audio samples from A to be
    let process_callback = move |_: &jack::Client, ps: &jack::ProcessScope| -> jack::Control {
        // For every output get a sum of corresponding inputs
        let mut output_buffers: [[f32; 1024]; N_OUTPUTS] = [[0.0; 1024]; N_OUTPUTS];

        // FIXME: calculate this per request and not on every buffer
        INPUT_CONNECTIONS.store(
            inputs
                .iter()
                .filter(|inport| port_has_connections(inport))
                .count(),
            Ordering::Relaxed,
        );
        OUTPUT_CONNECTIONS.store(
            outputs
                .iter()
                .filter(|outport| port_has_connections(outport))
                .count(),
            Ordering::Relaxed,
        );

        // Iterate over the inputs and add values to the corresponding outputs
        // (in this case for testing purposes: all to all)

        inputs
            .iter()
            .enumerate()
            .filter(|(_input_index, inport)| port_has_connections(inport))
            .for_each(|(input_index, inport)| {
                // Get a slice [f32; 1024] of samples for each port
                let input_samples = &inport.as_slice(ps)[..buffersize];

                // calculate and update peak
                input_meters[input_index].add_frames_f32(&input_samples).unwrap();
                let loudness = input_meters[input_index].prev_true_peak(0).unwrap() as f32;
                METERS[input_index]
                .fetch_update(
                    Ordering::Relaxed,
                    Ordering::Relaxed,
                    move |old_meter: f32| {
                        Some(old_meter.max(loudness))
                    },
                ).unwrap();

                // Sum each input to output buffer
                output_buffers
                    .par_iter_mut()
                    .enumerate()
                    .filter(|(output_index, _buffer)| port_has_connections(&outputs[*output_index]))
                    .map(|(output_index, buffer)| {
                        let volume = get_volume_for_input_output_pair(input_index, output_index);
                        (output_index, buffer, volume)
                    })
                    .filter(|(_, _, volume)| *volume != 0.0)
                    .for_each(|(_output_index, b, volume)| {
                        b.par_iter_mut().take(buffersize).enumerate().for_each(
                            |(sample_index, sample)| {
                                *sample = input_samples[sample_index].mul_add(volume, *sample);
                            },
                        );
                    });
            });

        // Iterate over the outputs and clone the values of the corresponding
        // output buffer to the output
        outputs
            .iter_mut()
            .enumerate()
            .for_each(|(output_index, outport)| {
                outport.as_mut_slice(ps)[..buffersize]
                    .clone_from_slice(&output_buffers[output_index][..buffersize]);
            });
        jack::Control::Continue
    };

    // Register the above closure with jack
    let process = jack::ClosureProcessHandler::new(process_callback);

    // Activate the client, which starts the processing.
    let active_client = client.activate_async(Notifications, process).unwrap();

    let server = rouille::Server::new(format!("{host}:{port}"), |request| {
        rouille::log(&request, io::stdout(), || {
            router!(request,
                (GET) (/volumes) => {
                    let data: VolumeData = handle_get_volume_request();

                    let mut response = rouille::Response::json(&data);
                    response.headers
                        .push(("Access-Control-Allow-Origin".into(), "*".into()));
                    response
                },
                (POST) (/volumes) => {
                    let data: VolumeData = try_or_400!(rouille::input::json_input(request));
                    handle_post_volume_request(data);

                    rouille::Response::empty_204()
                },
                (GET) (/ws) => {
                    let (response, websocket) = try_or_400!(rouille::websocket::start(&request, Some("meters")));

                    // Because of the nature of I/O in Rust, we need to spawn a separate thread for each websocket.
                    thread::spawn(move || {
                        // This line will block until the `response` above has been returned.
                        let ws = websocket.recv().unwrap();

                        handle_websocket_meters(ws);
                    });

                    response
                },
                (GET) (/health) => {
                    #[derive(Debug, Serialize)]
                    struct HealthData {
                        in_connections: usize,
                        out_connections: usize,
                    }
                    let in_connections = INPUT_CONNECTIONS.load(Ordering::Relaxed);
                    let out_connections = OUTPUT_CONNECTIONS.load(Ordering::Relaxed);

                    if in_connections > 0 && out_connections > 0 {
                        let mut response = rouille::Response::json(&HealthData {
                            in_connections,
                            out_connections,
                        });
                        response.headers
                            .push(("Access-Control-Allow-Origin".into(), "*".into()));
                        response
                    } else {
                        rouille::Response::text("No audio connections present.").with_status_code(500)
                    }

                },
                _ => rouille::Response::empty_404()
            )
        })
    }).unwrap();

    println!("Listening on {:?}", server.server_addr());
    server.run();

    // Wait for user input to quit
    println!("Press enter/return to quit...");
    let mut user_input = String::new();
    io::stdin().read_line(&mut user_input).ok();

    active_client.deactivate().unwrap();
}


fn handle_get_volume_request() -> VolumeData {
    let volumes = VOLS
        .iter()
        .enumerate()
        .map(|(input_index, row)| {
            row.iter()
                .enumerate()
                .map(|(output_index, _volume)| {
                    get_volume_for_input_output_pair(input_index, output_index)
                })
                .collect()
        })
        .collect::<Vec<Vec<f32>>>();
    VolumeData { volumes }
}

fn handle_post_volume_request(data: VolumeData) {
    data.volumes
        .iter()
        .enumerate()
        .for_each(|(input_index, row)| {
            row.iter().enumerate().for_each(|(output_index, volume)| {
                VOLS[input_index][output_index]
                    .store((volume * usize::MAX as f32) as usize, Ordering::Relaxed)
            })
        })
}

fn handle_websocket_meters(mut websocket: rouille::websocket::Websocket) {
    #[derive(Serialize)]
    struct MeterData {
        meters: Vec<f32>,
    }

    let interval = Duration::from_millis(100);
    let mut next_time = Instant::now() + interval;

    // FIXME: How do you do real connection handling?!?!?!
    let mut connection_open = true;
    while connection_open {
        let data = MeterData {
            meters: METERS
                .iter()
                .map(|meter| {
                    (meter.load(Ordering::Relaxed) * 100.0).round()
                        / 100.0
                })
                .collect::<Vec<f32>>(),
        };
        websocket
            .send_text(&serde_json::to_string::<MeterData>(&data).unwrap())
            .unwrap_or_else(|_| {
                connection_open = false;
                return;
            });

        if connection_open {
            thread::sleep(next_time - Instant::now());
            next_time += interval;
        }
    }
    println!("closed websocket!");
}

/// Notifications for Jack
struct Notifications;

impl jack::NotificationHandler for Notifications {
    fn thread_init(&self, _: &jack::Client) {
        // println!("JACK: thread init");
    }

    fn shutdown(&mut self, status: jack::ClientStatus, reason: &str) {
        println!(
            "JACK: shutdown with status {:?} because \"{}\"",
            status, reason
        );
    }

    fn freewheel(&mut self, _: &jack::Client, _is_enabled: bool) {
        // println!(
        //     "JACK: freewheel mode is {}",
        //     if is_enabled { "on" } else { "off" }
        // );
    }

    fn sample_rate(&mut self, _: &jack::Client, _srate: jack::Frames) -> jack::Control {
        // println!("JACK: sample rate changed to {}", srate);
        jack::Control::Continue
    }

    fn client_registration(&mut self, _: &jack::Client, _name: &str, _is_reg: bool) {
        // println!(
        //     "JACK: {} client with name \"{}\"",
        //     if is_reg { "registered" } else { "unregistered" },
        //     name
        // );
    }

    fn port_registration(&mut self, _: &jack::Client, _port_id: jack::PortId, _is_reg: bool) {
        // println!(
        //     "JACK: {} port with id {}",
        //     if is_reg { "registered" } else { "unregistered" },
        //     port_id
        // );
    }

    fn port_rename(
        &mut self,
        _: &jack::Client,
        _port_id: jack::PortId,
        _old_name: &str,
        _new_name: &str,
    ) -> jack::Control {
        // println!(
        //     "JACK: port with id {} renamed from {} to {}",
        //     port_id, old_name, new_name
        // );
        jack::Control::Continue
    }

    fn ports_connected(
        &mut self,
        _: &jack::Client,
        _port_id_a: jack::PortId,
        _port_id_b: jack::PortId,
        _are_connected: bool,
    ) {
        // println!(
        //     "JACK: ports with id {} and {} are {}",
        //     port_id_a,
        //     port_id_b,
        //     if are_connected {
        //         "connected"
        //     } else {
        //         "disconnected"
        //     }
        // );
    }

    fn graph_reorder(&mut self, _: &jack::Client) -> jack::Control {
        // println!("JACK: graph reordered");
        jack::Control::Continue
    }

    fn xrun(&mut self, _: &jack::Client) -> jack::Control {
        // println!("JACK: xrun occurred");
        jack::Control::Continue
    }
}
