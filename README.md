# hexmatrix

Forked from [elektronik/hexmatrix](https://code.hfbk.net/elektronik/hexmatrix).

Hexmatrix is a OSC-controlled 16x16 matrix mixer for the JACK audio system.

![](images/catia.jpg)

A matrix mixer allows you to send any input to any number of outputs. Internal computations are done at 32 bits.



## Usage

1. Start up hexmatrix and patch the jack connections to taste (e.g. using Catia)
2. Send http post requests to `/volumes` with the matrix as a two dimensional json array as the payload (see [./example_payload.json])

All `<volume>` values in the matrix should be a float, where a value of `0.0` equals -inf dB, `1.0` equals 0 dB and `8.0` equals roughly +18 dB.
If you are curious how to calculate this: `dB = 20.0 × log10(float)`.

### Running under pipewire

Set the desired buffer size & sample rate via the `PIPEWIRE_LATENCY` environment variable, e.g.

`PIPEWIRE_LATENCY=256/48000 cargo run`

## Endpoints

- GET `/volumes`
- POST `/volumes`
- WebSockets `/ws`
- GET `/health`

## Installation

1. Clone this repository with git
2. Make sure you have rust installed then within the project directory run `cargo build --release`
3. Grab the resulting hexmatrix binary from the `./target` directory
4. Run it
